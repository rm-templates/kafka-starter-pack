# KAFKA STARTER PACK

A simple setup for testing purposes with kafka that depends on the bitnami Kakfka Helm Chart and will also launch a kafka-ui along with it

## Installation

To install it use

```sh
helm install kafka -n kafka --create-namespace ./kafka
```
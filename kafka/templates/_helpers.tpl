{{- define "customName" -}}
{{- if not (contains "kafka" .Release.Name) -}}
{{- printf "%s-kafka" .Release.Name -}}
{{- else -}}
{{- .Release.Name -}}
{{- end -}}
{{- end -}}